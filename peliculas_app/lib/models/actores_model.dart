class Cast{
  List<Actor> actores = new List();

  Cast.fromJsonList(List<dynamic> jsonList){
    if(jsonList == null) return;

    jsonList.forEach((element) {
      final actor = Actor.fromJsonMap(element);
      actores.add(actor);
    });
  }
}

class Actor{
  int castId;
  String character;
  String creditId;
  int gender;
  int id;
  String name;
  int order;
  String profilePath;

  Actor({
      this.castId,
      this.character,
      this.creditId,
      this.gender,
      this.id,
      this.name,
      this.order,
      this.profilePath
      });

  Actor.fromJsonMap(Map<String,dynamic> json){
    castId    = json['cast_id'];
    character = json['character'];
    creditId  = json['credit_id'];
    gender    = json['gender'];
    id= json['id'];
    name= json['name'];
    order= json['order'];
    profilePath= json['profile_path'];
  }

  getImg() {
    if (profilePath == null) {
      return 'https://www.google.com/url?sa=i&url=https%3A%2F%2Fdepositphotos.com%2Fvector-images%2Fanonymous-profile-pic.html&psig=AOvVaw32pPB0PzK4XkBhCKXfmp_5&ust=1613681631016000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCMDzmtbm8e4CFQAAAAAdAAAAABAO';
    } else {
      return "https://image.tmdb.org/t/p/w500/$profilePath";
    }
  }

}
