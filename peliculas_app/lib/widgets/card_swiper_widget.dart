import 'package:flutter/cupertino.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:peliculas_app/models/pelicula_model.dart';

class CardSwiperWidget extends StatelessWidget {
  final List<Pelicula> peliculas;

  CardSwiperWidget({@required this.peliculas});

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    return Container(
        padding: EdgeInsets.only(top: 10.0),
        width: _screenSize.width * 0.98,
        height: _screenSize.height * 0.5,
        child: Swiper(
          layout: SwiperLayout.STACK,
          itemWidth: 250.0,
          itemBuilder: (BuildContext context, int index) {

          peliculas[index].uniqueId = '${peliculas[index].id}-tarjeta';



            return Hero(
              tag: peliculas[index].id,
                child: ClipRRect(
              borderRadius: BorderRadius.circular(30.0),
              child: GestureDetector(
                onTap: () => Navigator.pushNamed(context, 'detalle',arguments: peliculas[index]),
                  child: FadeInImage(
                image:  NetworkImage(peliculas[index].getPosterPath()),
                placeholder: NetworkImage('https://www.allianceplast.com/wp-content/uploads/2017/11/no-image.png'),
                fit: BoxFit.cover,
              )),
            ));
          },
          itemCount: peliculas.length,
          //pagination: new SwiperPagination(),
          //control: new SwiperControl(),
        ));
  }
}
