import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:peliculas_app/models/actores_model.dart';
import 'dart:convert';
import 'package:peliculas_app/models/pelicula_model.dart';

class PeliculasProvider {
  String _apiKey = '4180f7444b23831911c3a9fd47b305cd';
  String _languaje = 'es-ES';
  String _url = 'api.themoviedb.org';

  int _popularesPage = 0;
  bool _cargando = false;

  List<Pelicula> _populares = new List();

  final _popularesStreamController = StreamController<List<Pelicula>>.broadcast();

  Function(List<Pelicula>) get popularesSink => _popularesStreamController.sink.add;

  Stream<List<Pelicula>> get popularesStream => _popularesStreamController.stream;

  void disposeStreams(){
    _popularesStreamController?.close();
  }

  Future<List<Pelicula>> getEnCines() async {
    final resp = await http.get(Uri.https(_url, '3/movie/now_playing',
        {'api_key': _apiKey, 'language': _languaje}));

    final decodeData = json.decode(resp.body);
    final peliculas = new Peliculas.fromJsonList(decodeData['results']);
    print(peliculas.items[1].title);
    return peliculas.items;
  }

  Future<List<Pelicula>> getPopulares() async {

    if(_cargando){return [];}
    _cargando = true;

    _popularesPage++;
    //print('cargando siguiente');
    final resp = await http.get(Uri.https(_url, '3/movie/popular',
        {'api_key': _apiKey, 'language': _languaje, 'page': _popularesPage.toString()}));

    final decodeData = json.decode(resp.body);
    final peliculas = new Peliculas.fromJsonList(decodeData['results']);
    print(peliculas.items[1].title);
    _populares.addAll(peliculas.items);
    popularesSink(_populares);
    _cargando = false;
    return peliculas.items;
  }

  Future<List<Actor>> getCast(String peliId) async{
    final resp = await http.get(Uri.https(_url, '3/movie/$peliId/credits',
        {'api_key': _apiKey, 'language': _languaje}));

    final decodeData = json.decode(resp.body);
    final cast = new Cast.fromJsonList(decodeData['cast']);
    return cast.actores;
  }




}
