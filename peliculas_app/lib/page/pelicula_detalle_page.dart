

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:peliculas_app/models/actores_model.dart';
import 'package:peliculas_app/models/pelicula_model.dart';
import 'package:peliculas_app/providers/peliculas_providers.dart';

class PeliculaDetalle extends StatelessWidget{


  @override
  Widget build(BuildContext context) {

    final Pelicula pelicula = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      body: CustomScrollView(
        slivers: [
          _crearAppbar(pelicula),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                SizedBox(height: 10.0),
                _posterTitle(pelicula, context),
                _description(pelicula),
                _description(pelicula),
                _description(pelicula),
                _description(pelicula),
                _description(pelicula),
                _crearCasting(pelicula)
              ]
            ),
          )
        ],
      ),
    );
  }

  _crearAppbar(Pelicula pelicula) {
    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Colors.brown,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(
          pelicula.title,
          style: TextStyle(
            color: Colors.white, fontSize: 16.0
          ),
        ),
        background: FadeInImage(
          image:NetworkImage(pelicula.getBackgroundImg()),
        placeholder: NetworkImage('https://www.allianceplast.com/wp-content/uploads/2017/11/no-image.png'),
        fadeInDuration: Duration(microseconds: 150),
        fit: BoxFit.cover
        ),
      ),
    );
  }

  _posterTitle(Pelicula pelicula, BuildContext context) {
    return Container(
      child: Row(
        children: [
          Hero( tag: pelicula.uniqueId,
              child: ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
              child:Image(image: NetworkImage(pelicula.getPosterPath()),
          height: 150.0,
          ))),
          SizedBox(width: 20.0),
          Flexible(child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(pelicula.title,style: Theme.of(context).textTheme.title, overflow: TextOverflow.ellipsis),
              Text(pelicula.originalTitle, style: Theme.of(context).textTheme.subhead, overflow: TextOverflow.ellipsis),
              Row(
                children: [
                  Icon(Icons.star_border),
                  Text(pelicula.voteAverage.toString())
                  
                ],
              )
            ],
          ))

        ],
      ),
    );
  }

  _description(Pelicula pelicula) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      child: Text(pelicula.overview, textAlign: TextAlign.justify,),
    );
  }

  _crearCasting(Pelicula pelicula) {
    final peliProvider = new PeliculasProvider();
    return FutureBuilder(
        future: peliProvider.getCast(pelicula.id.toString()),
      builder: (BuildContext context, AsyncSnapshot<List> snap) {
          if(snap.hasData){
            return _crearActoresPageView(snap.data);
          }
          else{
            return Center(child: CircularProgressIndicator());
          }
          return null;
      }
      ,
    );
  }

  _crearActoresPageView(List<Actor> data) {
    return SizedBox(
      height: 200.0,
      child: PageView.builder(
        controller: PageController(
          viewportFraction: 0.3,
          initialPage: 1
        ),
          itemCount: data.length,
          itemBuilder: (context,i)=> _actorTarjeta(data[i])
      ),
    );
  }

  _actorTarjeta(Actor actor){
    return Container(
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
              child:FadeInImage(
                  placeholder: AssetImage('resources/no-image.jpg'),
                  image: NetworkImage(actor.getImg()),
              height: 150.0,
              fit: BoxFit.cover,)),
          Text(actor.name,
          overflow: TextOverflow.ellipsis),
        ],
      ),
    );
  }

}