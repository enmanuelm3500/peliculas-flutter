import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:peliculas_app/providers/peliculas_providers.dart';
import 'package:peliculas_app/search/search_delegate.dart';
import 'package:peliculas_app/widgets/card_swiper_widget.dart';
import 'package:peliculas_app/widgets/movie_horizontal.dart';

class HomePage extends StatelessWidget {
  final PeliculasProvider provider = new PeliculasProvider();

  @override
  Widget build(BuildContext context) {
    provider.getPopulares();

    return Scaffold(
        appBar: AppBar(
          centerTitle: false,
          title: Text('Peliculas'),
          backgroundColor: Colors.brown,
          actions: [IconButton(icon: Icon(Icons.search), onPressed: () {
            showSearch(context: context, delegate: DataSearch(),
                //query: 'hola'
            );
          })],
        ),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [_swiperTarjetas(), _footer(context)],
          ),
        ));
  }

  _swiperTarjetas() {
    return FutureBuilder(
      future: provider.getEnCines(),
      builder:
          (BuildContext context, AsyncSnapshot<List<dynamic>> asyncSnapshot) {
        if (asyncSnapshot.hasData) {
          return CardSwiperWidget(peliculas: asyncSnapshot.data);
        } else {
          return Container(
              height: 400,
              child: Center(
                child: CircularProgressIndicator(),
              ));
        }
      },
    );
  }

  _footer(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: [
          SizedBox(height: 5.0),

          Text(
            'Populares',

            style: Theme
                .of(context)
                .textTheme
                .subtitle1,
          ),
          StreamBuilder(
              builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                if (snapshot.hasData) {
                  return MovieHorizontalPage(peliculas: snapshot.data, siguientePag: provider.getPopulares);
                  }else{
                  return CircularProgressIndicator( );
                }

                },
              stream: provider.popularesStream),

        ],
      ),
    );
  }
}
