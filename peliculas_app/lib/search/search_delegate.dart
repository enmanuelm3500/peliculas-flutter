import 'package:flutter/material.dart';

class DataSearch extends SearchDelegate{

  String seleccion = '';
  final peliculas = ['Spiderman', 'Batman', 'IronMan 1', 'IronMan 2', 'IronMan 3'];
  final peliculasSugeridas = ['Spiderman', 'Capitan America'];


  @override
  List<Widget> buildActions(BuildContext context) {
   return [
     IconButton(icon: Icon(Icons.clear), onPressed: () => query = '')
   ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation,
        ),
       onPressed: () => close(context, null));

  }

  @override
  Widget buildResults(BuildContext context) {
    return Container(

    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {

    final listaSugerida = (query.isEmpty)? peliculasSugeridas:peliculas.where((element) => element.toLowerCase().startsWith(query.toLowerCase())).toList();

    return ListView.builder(
        itemCount: listaSugerida.length,
        itemBuilder: (context,i) {
      return ListTile(
        leading: Icon(Icons.movie),
        title: Text(listaSugerida[i]),
        onTap: (){
          seleccion = listaSugerida[i];
          showResults(context);
        },

      );
    });
  }
  
  
  
}